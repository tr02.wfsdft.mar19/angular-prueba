import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  url:'http://localhost:4200/assets/temario.json';

  constructor(private http: HttpClient) { }

  getTemario() {
    return this.http.get<any[]>(this.url);
  }
}
