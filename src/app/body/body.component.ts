import { Component, OnInit } from '@angular/core';
import { BackendService } from '../servicios/backend.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
titulo = "Hola Body";
temario = [];
databody: any = {
  title: "curso de Angular",
  subtitle: "Una breve introducción a Angular"
};

  constructor(private backend: BackendService) { }

  ngOnInit() {
    this.temarioList();
  }

  temarioList() {
    this.backend.getTemario().subscribe(
      datos => this.temario = datos
    );
  };

};
